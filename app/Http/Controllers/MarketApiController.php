<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\MarketService;
use App\Http\Resources\ProductResource;

class MarketApiController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function showList()
    {
        $products = $this->marketService->getProductList();

        return ProductResource::collection($products);
    }

    public function store(Request $request)
    {
        $validator = $this->prepareValidator();

        if ($validator->fails()) {
            return new Response([
                'message' => 'fail',
                'errors' => $validator->errors()->toArray(),
            ], 400);
        }

        $product = $this->marketService->storeProduct($request);

        return new ProductResource($product);
    }

    public function showProduct(int $id)
    {
        try {
            $product = $this->marketService->getProductByID($id);

            return new ProductResource($product);
        } catch (\Exception $e) {
            return new Response([
                'message' => 'fail',
                'errors' => $e->getMessage(),
            ], 400);
        }
    }

    public function delete(Request $request)
    {
        try {
            $result = $this->marketService->deleteProduct($request);

            if ($result) {
                return new Response([], 204);
            }

            return new Response([
                'message' => 'Forbidden.',
            ], 403);
        } catch (\Exception $e) {
            return new Response([
                'message' => 'fail',
                'errors' => $e->getMessage(),
            ], 400);
        }
    }

    protected function prepareValidator()
    {
        return validator(request()->all(), [
            'name' => ['required', 'string', 'between:3,255'],
            'price' => ['required', 'numeric', 'between:0.01,999999.99'],
        ]);
    }
}
