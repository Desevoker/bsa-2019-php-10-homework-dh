<?php

namespace Tests\Feature;

use App\Entities\User;
use App\Entities\Product;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class MarketApiTest extends TestCase
{
    use DatabaseMigrations;

    protected function setUp(): void
    {
        parent::setUp();

        factory(User::class, 3)
            ->create()
            ->each(function ($user) {
                $user->products()->saveMany(factory(Product::class, 5)->make());
            });
    }

    /** @test */
    public function get_all_products()
    {
        $products = $this->json('get', '/api/items')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(200)
            ->assertJsonStructure([
                '*' => [
                    'id',
                    'name',
                    'price',
                    'user_id',
                ],
            ])
            ->assertJsonCount(15)
            ->json();

        $this->assertNotEmpty($products);
        $this->assertCount(15, $products);

        foreach ($products as $item) {
            $this->assertArrayHasKey('id', $item);
            $this->assertArrayHasKey('name', $item);
            $this->assertArrayHasKey('price', $item);
            $this->assertArrayHasKey('user_id', $item);
        }
    }

    /** @test */
    public function get_product_by_id()
    {
        $this->json('get', '/api/items/5')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(200)
            ->assertJsonStructure([
                'id',
                'name',
                'price',
                'user_id',
            ]);
    }

    /** @test */
    public function cant_get_not_existing_product_by_id()
    {
        $this->json('get', '/api/items/25')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(400)
            ->assertJsonFragment([
                'message' => 'fail',
                'errors' => 'No product with id: 25',
            ]);
    }

    /** @test */
    public function user_can_create_a_product()
    {
        $user = User::find(1);
        $this->actingAs($user, 'api');

        $this->json('post', '/api/items', [
                'name' => 'New product',
                'price' => 25.59,
            ])
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(201)
            ->assertExactJson([
                'id' => 16,
                'name' => 'New product',
                'price' => 25.59,
                'user_id' => 1,
            ]);

        $this->assertDatabaseHas('products', [
            'id' => 16,
            'name' => 'New product',
            'price' => 25.59,
            'user_id' => 1,
        ]);
    }

    /** @test */
    public function user_cannot_create_a_product_with_invalid_data()
    {
        $user = User::find(1);
        $this->actingAs($user, 'api');

        $this->json('post', '/api/items', [
                'name' => 10,
                'price' => 'some price',
            ])
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(400)
            ->assertJsonFragment([
                'message' => 'fail',
            ])
            ->assertJsonValidationErrors([
                'name',
                'price',
            ]);
    }

    /** @test */
    public function guest_cannot_create_a_product()
    {
        $this->json('post', '/api/items', [
                'name' => 'Guest product',
                'price' => 15.99,
            ])
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(401)
            ->assertJsonFragment([
                'message' => 'Unauthenticated.',
            ]);
    }

    /** @test */
    public function user_can_delete_his_product()
    {
        $user = User::find(1);
        $this->actingAs($user, 'api');

        $this->json('delete', '/api/items/1')
            ->assertStatus(204);

        $this->assertDatabaseMissing('products', ['id' => 1]);
    }

    /** @test */
    public function user_cannot_delete_not_existing_product()
    {
        $user = User::find(1);
        $this->actingAs($user, 'api');

        $this->json('delete', '/api/items/25')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(400)
            ->assertJsonFragment([
                'message' => 'fail',
                'errors' => 'No product with id: 25',
            ]);
    }

    /** @test */
    public function user_cannot_delete_another_user_product()
    {
        $user = User::find(1);
        $this->actingAs($user, 'api');

        $this->json('delete', '/api/items/10')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(403)
            ->assertJsonFragment([
                'message' => 'Forbidden.',
            ]);
    }

    /** @test */
    public function guest_cannot_delete_a_product()
    {
        $this->json('delete', '/api/items/10')
            ->assertHeader('Content-Type', 'application/json')
            ->assertStatus(401)
            ->assertJsonFragment([
                'message' => 'Unauthenticated.',
            ]);
    }
}
