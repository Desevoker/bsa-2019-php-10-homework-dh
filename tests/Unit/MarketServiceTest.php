<?php

namespace Tests\Unit;

use App\Entities\User;
use App\Entities\Product;
use App\Services\MarketService;
use App\Repositories\ProductRepository;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Tests\TestCase;

class MarketServiceTest extends TestCase
{
    protected $marketService;

    protected function setUp(): void
    {
        parent::setUp();

        $repository = $this->createMock(ProductRepository::class);

        $repository->method('findAll')
            ->will(
                $this->returnCallback(function () {
                    return $this->findAll();
                })
            );

        $repository->method('findById')
            ->will(
                $this->returnCallback(function ($id) {
                    return $this->findById($id);
                })
            );

        $repository->method('findByUserId')
            ->will(
                $this->returnCallback(function ($id) {
                    return $this->findByUserId($id);
                })
            );

        $repository->method('store')
            ->will(
                $this->returnArgument(0)
            );

        $this->app->instance(ProductRepositoryInterface::class, $repository);

        $this->marketService = $this->app->make(MarketService::class);
    }

    /** @test */
    public function get_all_products()
    {
        $products = $this->marketService->getProductList();

        $this->assertInstanceOf(Collection::class, $products);
        $this->assertNotEmpty($products);
        $this->assertContainsOnlyInstancesOf(Product::class, $products);
        $this->assertCount(5, $products);
    }

    /** @test */
    public function get_product_by_id()
    {
        $product = $this->marketService->getProductById(4);

        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals(4, $product->id);
        $this->assertEquals('Fourth product', $product->name);
        $this->assertEquals(10.99, $product->price);
        $this->assertEquals(2, $product->user_id);
    }

    /** @test */
    public function cant_get_not_existing_product_by_id()
    {
        try {
            $this->marketService->getProductById(10);
        } catch (\Exception $e) {
            $this->assertStringContainsString('No product with id: 10', $e->getMessage());
        }
    }

    /** @test */
    public function get_products_by_user_id()
    {
        $products = $this->marketService->getProductsByUserId(1);

        $this->assertInstanceOf(Collection::class, $products);
        $this->assertNotEmpty($products);
        $this->assertContainsOnlyInstancesOf(Product::class, $products);
        $this->assertCount(3, $products);
        $this->assertEquals(1, $products->first()->user_id);
        $this->assertEquals(1, $products->last()->user_id);
    }

    /** @test */
    public function cant_get_products_by_not_existing_user_id()
    {
        $products = $this->marketService->getProductsByUserId(5);

        $this->assertInstanceOf(Collection::class, $products);
        $this->assertEmpty($products);
        $this->assertCount(0, $products);
    }

    /** @test */
    public function create_new_product()
    {
        $user = factory(User::class)->make(['id' => 3]);

        $this->actingAs($user);

        $request = $this->createMock(Request::class);

        $request->expects($this->exactly(2))
            ->method('input')
            ->withConsecutive(
                [$this->stringContains('name')],
                [$this->stringContains('price')]
            )
            ->will(
                $this->onConsecutiveCalls('New product', 20)
            );

        $product = $this->marketService->storeProduct($request);

        $this->assertInstanceOf(Product::class, $product);
        $this->assertEquals('New product', $product->name);
        $this->assertEquals(20, $product->price);
        $this->assertEquals(3, $product->user_id);
    }

    protected function findByUserId(int $id): Collection
    {
        return $this->findAll()->where('user_id', $id);
    }

    protected function findById(int $id): ?Product
    {
        return $this->findAll()->where('id', $id)->first();
    }

    protected function findAll(): Collection
    {
        return collect($this->productsData());
    }

    protected function productsData(): array
    {
        return [
            new Product([
                'id' => 1,
                'name' => 'First product',
                'price' => 2.99,
                'user_id' => 1,
            ]),
            new Product([
                'id' => 2,
                'name' => 'Second product',
                'price' => 6.99,
                'user_id' => 1,
            ]),
            new Product([
                'id' => 3,
                'name' => 'Third product',
                'price' => 4.99,
                'user_id' => 1,
            ]),
            new Product([
                'id' => 5,
                'name' => 'Fifth product',
                'price' => 8.99,
                'user_id' => 2,
            ]),
            new Product([
                'id' => 4,
                'name' => 'Fourth product',
                'price' => 10.99,
                'user_id' => 2,
            ]),
        ];
    }
}
